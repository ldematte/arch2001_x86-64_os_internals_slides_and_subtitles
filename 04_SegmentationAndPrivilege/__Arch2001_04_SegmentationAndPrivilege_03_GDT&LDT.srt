1
00:00:00,160 --> 00:00:04,960
So let's learn more about these gdt and

2
00:00:02,639 --> 00:00:06,480
ldt tables that we learned about that we

3
00:00:04,960 --> 00:00:07,520
saw references to in the previous

4
00:00:06,480 --> 00:00:09,760
section

5
00:00:07,520 --> 00:00:11,599
so the segment selector is what we saw

6
00:00:09,760 --> 00:00:13,440
and we saw there was a table indicator

7
00:00:11,599 --> 00:00:15,519
bit in there where if it was zero it was

8
00:00:13,440 --> 00:00:17,279
pointing at the gdt and if it was one it

9
00:00:15,519 --> 00:00:19,600
was pointing at the ldt

10
00:00:17,279 --> 00:00:21,439
so we're going to dig more into that so

11
00:00:19,600 --> 00:00:23,279
while this table indicator says which of

12
00:00:21,439 --> 00:00:25,840
these tables it's pointing at

13
00:00:23,279 --> 00:00:28,400
how we find the actual tables themselves

14
00:00:25,840 --> 00:00:31,039
are with these special purpose registers

15
00:00:28,400 --> 00:00:33,120
there's a gdt register which points at a

16
00:00:31,039 --> 00:00:34,480
memory location that says here's the

17
00:00:33,120 --> 00:00:36,960
memory for the gdt

18
00:00:34,480 --> 00:00:38,719
and there's an ldt register that points

19
00:00:36,960 --> 00:00:40,239
at a memory location that says here's

20
00:00:38,719 --> 00:00:42,239
the ldt

21
00:00:40,239 --> 00:00:43,840
furthermore this ldt register even

22
00:00:42,239 --> 00:00:44,879
though it was shown as if it had all

23
00:00:43,840 --> 00:00:47,200
these fields

24
00:00:44,879 --> 00:00:48,320
in practice it sort of behaves like a

25
00:00:47,200 --> 00:00:50,960
segment register

26
00:00:48,320 --> 00:00:52,800
and that there is only a 16-bit area

27
00:00:50,960 --> 00:00:55,039
that's actually visible to us

28
00:00:52,800 --> 00:00:57,360
and that can be set and then there is a

29
00:00:55,039 --> 00:00:58,000
hidden portion that acts as a cache of

30
00:00:57,360 --> 00:01:00,000
information

31
00:00:58,000 --> 00:01:02,160
coming from the other table and then the

32
00:01:00,000 --> 00:01:03,440
entries in these tables are called

33
00:01:02,160 --> 00:01:05,199
segment descriptors

34
00:01:03,440 --> 00:01:06,880
but we're not going to learn about them

35
00:01:05,199 --> 00:01:08,960
until the next section they're basically

36
00:01:06,880 --> 00:01:11,760
data structures describing the segments

37
00:01:08,960 --> 00:01:13,439
that are in these tables

38
00:01:11,760 --> 00:01:14,880
let's look at that first register the

39
00:01:13,439 --> 00:01:17,840
global descriptor table

40
00:01:14,880 --> 00:01:18,240
register so this register is 10 bytes

41
00:01:17,840 --> 00:01:21,280
long

42
00:01:18,240 --> 00:01:22,799
it has two bytes for the table limit

43
00:01:21,280 --> 00:01:26,159
that says the size

44
00:01:22,799 --> 00:01:27,360
and eight bytes for a 64-bit linear base

45
00:01:26,159 --> 00:01:29,840
address

46
00:01:27,360 --> 00:01:31,439
so that is a linear address and we said

47
00:01:29,840 --> 00:01:33,600
for now we're pretending paging doesn't

48
00:01:31,439 --> 00:01:35,600
exist so that effectively is a physical

49
00:01:33,600 --> 00:01:37,680
address as far as we're concerned

50
00:01:35,600 --> 00:01:39,439
so 64-bit address of the start of the

51
00:01:37,680 --> 00:01:42,560
table and a 2-byte

52
00:01:39,439 --> 00:01:45,200
address size of the end of the table

53
00:01:42,560 --> 00:01:46,399
now this table limit field is actually a

54
00:01:45,200 --> 00:01:49,119
size in bytes

55
00:01:46,399 --> 00:01:49,840
and it is a size that specifies the last

56
00:01:49,119 --> 00:01:52,799
valid

57
00:01:49,840 --> 00:01:55,520
or included byte in the table so if the

58
00:01:52,799 --> 00:01:57,680
overall table was hex 1000 big

59
00:01:55,520 --> 00:01:59,360
this wouldn't say hex 1000 it would say

60
00:01:57,680 --> 00:02:02,399
hex fff

61
00:01:59,360 --> 00:02:05,520
to basically say the last byte is

62
00:02:02,399 --> 00:02:07,360
this base address plus fff

63
00:02:05,520 --> 00:02:08,560
that's the last byte that you can index

64
00:02:07,360 --> 00:02:11,599
into

65
00:02:08,560 --> 00:02:14,640
now setting this register is done

66
00:02:11,599 --> 00:02:17,520
via a special instruction lgdt

67
00:02:14,640 --> 00:02:19,360
load the gdt register and you can see

68
00:02:17,520 --> 00:02:21,360
this is a privileged instruction which

69
00:02:19,360 --> 00:02:24,080
can only be done by the kernel

70
00:02:21,360 --> 00:02:25,120
and reading or storing out the contents

71
00:02:24,080 --> 00:02:28,560
of that register

72
00:02:25,120 --> 00:02:31,440
can be done via the sgdt store gdt

73
00:02:28,560 --> 00:02:33,920
to memory instruction and that actually

74
00:02:31,440 --> 00:02:35,840
is not a privileged instruction

75
00:02:33,920 --> 00:02:37,200
now there's really no good reason why

76
00:02:35,840 --> 00:02:38,959
someone other than the colonel would

77
00:02:37,200 --> 00:02:40,239
need to actually read out the contents

78
00:02:38,959 --> 00:02:42,160
of this register

79
00:02:40,239 --> 00:02:43,280
but there's an interesting dichotomy

80
00:02:42,160 --> 00:02:45,519
that we'll

81
00:02:43,280 --> 00:02:47,440
see exploited for interesting purposes

82
00:02:45,519 --> 00:02:49,040
later on given the fact that this

83
00:02:47,440 --> 00:02:50,480
you know one of these instructions for

84
00:02:49,040 --> 00:02:53,840
writing is privileged but one of these

85
00:02:50,480 --> 00:02:55,840
instructions for reading is not.

86
00:02:53,840 --> 00:02:57,519
So let's go ahead and just go into the

87
00:02:55,840 --> 00:03:00,879
debugger and look at

88
00:02:57,519 --> 00:03:02,400
the gdt register now it turns out that

89
00:03:00,879 --> 00:03:05,040
there's a Windbg-ism

90
00:03:02,400 --> 00:03:06,400
in that although the register itself is

91
00:03:05,040 --> 00:03:08,560
10 bytes big

92
00:03:06,400 --> 00:03:09,760
uh Windbg actually sort of breaks up

93
00:03:08,560 --> 00:03:12,800
your view into it

94
00:03:09,760 --> 00:03:15,840
to what they call the gdtr is

95
00:03:12,800 --> 00:03:16,480
actually only the upper 64-bit base

96
00:03:15,840 --> 00:03:18,000
address

97
00:03:16,480 --> 00:03:20,000
and what they call this sort of

98
00:03:18,000 --> 00:03:22,640
pseudo-register gdtl

99
00:03:20,000 --> 00:03:23,920
is the lower limit address so let's go

100
00:03:22,640 --> 00:03:26,159
ahead and look at that let's figure out

101
00:03:23,920 --> 00:03:29,040
what the base address is of the gdt

102
00:03:26,159 --> 00:03:31,200
and what the size of the gdt is based on

103
00:03:29,040 --> 00:03:34,799
this register

104
00:03:31,200 --> 00:03:37,360
so I'm going to go into my debugger

105
00:03:34,799 --> 00:03:38,159
and I break into the kernel loop got to

106
00:03:37,360 --> 00:03:41,920
wake up my

107
00:03:38,159 --> 00:03:44,480
VM as well so wake up my VM

108
00:03:41,920 --> 00:03:45,840
break into the debugger and if you

109
00:03:44,480 --> 00:03:46,640
scroll all the way down in your

110
00:03:45,840 --> 00:03:48,799
registers

111
00:03:46,640 --> 00:03:49,680
you could customize it and put the gdt

112
00:03:48,799 --> 00:03:51,200
at the beginning

113
00:03:49,680 --> 00:03:53,200
but if you just go all the way down

114
00:03:51,200 --> 00:03:54,640
you'll see that the gdt register is

115
00:03:53,200 --> 00:03:56,959
actually at the bottom

116
00:03:54,640 --> 00:03:58,080
so if we scroll all the way down we see

117
00:03:56,959 --> 00:04:00,720
gdtr

118
00:03:58,080 --> 00:04:01,439
is some address that looks like a kernel

119
00:04:00,720 --> 00:04:03,040
address

120
00:04:01,439 --> 00:04:04,400
we haven't covered canonical addresses

121
00:04:03,040 --> 00:04:04,959
yet but it looks like kernel addressed

122
00:04:04,400 --> 00:04:07,040
me and

123
00:04:04,959 --> 00:04:08,000
you'll see later why you can kind of

124
00:04:07,040 --> 00:04:10,080
immediately

125
00:04:08,000 --> 00:04:11,519
determine that that's a kernel address

126
00:04:10,080 --> 00:04:13,439
so 64-bit address

127
00:04:11,519 --> 00:04:16,000
that is the base address that is where

128
00:04:13,439 --> 00:04:19,040
in memory we can find the gdt table

129
00:04:16,000 --> 00:04:21,759
and if we wanted we could do a dd

130
00:04:19,040 --> 00:04:23,199
gdtr and we would just be looking at

131
00:04:21,759 --> 00:04:24,720
some bytes in this table we don't know

132
00:04:23,199 --> 00:04:27,440
how to interpret them yet but

133
00:04:24,720 --> 00:04:28,800
there's some data structures in there

134
00:04:27,440 --> 00:04:32,160
and the gdtl

135
00:04:28,800 --> 00:04:35,360
is 57. So I said that the the limit

136
00:04:32,160 --> 00:04:37,759
is the size in bytes of the lat and it

137
00:04:35,360 --> 00:04:40,400
includes the last byte of the thing so

138
00:04:37,759 --> 00:04:41,120
if it's 57 that means it's a total of 58

139
00:04:40,400 --> 00:04:43,919
bytes just

140
00:04:41,120 --> 00:04:45,280
indexed 0 through 57. so whatever this

141
00:04:43,919 --> 00:04:47,520
data structure is

142
00:04:45,280 --> 00:04:49,440
it should be considered to be done after

143
00:04:47,520 --> 00:04:52,720
58 bytes.

144
00:04:49,440 --> 00:04:55,360
All right so that is the gdt register.

145
00:04:52,720 --> 00:04:56,000
so now let's look at the local

146
00:04:55,360 --> 00:04:59,759
descriptor

147
00:04:56,000 --> 00:05:03,039
table register, ldtr. So I said that this

148
00:04:59,759 --> 00:05:03,759
is a register that has a 16-bit segment

149
00:05:03,039 --> 00:05:05,440
selector

150
00:05:03,759 --> 00:05:06,880
like we already learned about which you

151
00:05:05,440 --> 00:05:10,160
had seen placed

152
00:05:06,880 --> 00:05:12,560
into the segment registers like cs, ds, es

153
00:05:10,160 --> 00:05:13,199
etc and so it has a 16-bit segment

154
00:05:12,560 --> 00:05:15,120
selector

155
00:05:13,199 --> 00:05:18,240
and then it has a hidden portion just

156
00:05:15,120 --> 00:05:20,240
like those css registers

157
00:05:18,240 --> 00:05:22,479
the hidden portion again acts as a sort

158
00:05:20,240 --> 00:05:25,199
of cache to take information from the

159
00:05:22,479 --> 00:05:26,880
table which here will be the gdt table

160
00:05:25,199 --> 00:05:28,320
and cache it here so that it doesn't

161
00:05:26,880 --> 00:05:30,080
have to be looked up from memory every

162
00:05:28,320 --> 00:05:32,639
time

163
00:05:30,080 --> 00:05:33,520
so the segment selector itself because

164
00:05:32,639 --> 00:05:35,600
it is a

165
00:05:33,520 --> 00:05:37,680
you know has a table indicator bit table

166
00:05:35,600 --> 00:05:40,400
indicator bit must always point at zero

167
00:05:37,680 --> 00:05:42,720
to say it selects from the gdt because

168
00:05:40,400 --> 00:05:45,039
the ldtr is how you find the ldt

169
00:05:42,720 --> 00:05:47,520
and so you can't have a self-rent

170
00:05:45,039 --> 00:05:49,840
referential thing saying oh yeah I'm the

171
00:05:47,520 --> 00:05:52,880
ldtr and I point at the ldt otherwise

172
00:05:49,840 --> 00:05:55,440
you wouldn't be able to find the ldt.

173
00:05:52,880 --> 00:05:57,039
So just like the gdtr there are special

174
00:05:55,440 --> 00:06:00,080
assembly instructions to

175
00:05:57,039 --> 00:06:02,800
read and write. The lldt is

176
00:06:00,080 --> 00:06:03,840
load a 16 bit segment selector, so again

177
00:06:02,800 --> 00:06:05,680
you can only

178
00:06:03,840 --> 00:06:07,840
hit this visible portion, you can only

179
00:06:05,680 --> 00:06:11,360
load up the visible portion

180
00:06:07,840 --> 00:06:13,280
of the register and then sldt is again

181
00:06:11,360 --> 00:06:15,680
something that's not privileged and

182
00:06:13,280 --> 00:06:17,360
which will take the 16-bit value and

183
00:06:15,680 --> 00:06:19,360
store it out to memory if someone wants

184
00:06:17,360 --> 00:06:21,600
to read it

185
00:06:19,360 --> 00:06:23,199
so let's go ahead and look at the ldt

186
00:06:21,600 --> 00:06:25,919
register and see

187
00:06:23,199 --> 00:06:27,759
what it uses as the gdt index we said

188
00:06:25,919 --> 00:06:28,560
ldt register is a 16-bit segment

189
00:06:27,759 --> 00:06:30,319
selector

190
00:06:28,560 --> 00:06:31,840
we covered segment selectors in the

191
00:06:30,319 --> 00:06:32,639
previous section we know they have an

192
00:06:31,840 --> 00:06:35,280
RPL

193
00:06:32,639 --> 00:06:38,160
table indicator and an index so let's see

194
00:06:35,280 --> 00:06:38,160
what that index is.

195
00:06:39,919 --> 00:06:44,080
All right, I break in my debugger

196
00:06:45,840 --> 00:06:49,599
and if we scroll all the way down we

197
00:06:47,520 --> 00:06:52,160
find the ldt register

198
00:06:49,599 --> 00:06:52,800
and it seems to be set to zero so you

199
00:06:52,160 --> 00:06:56,479
know what

200
00:06:52,800 --> 00:07:00,160
is zero well zero is actually

201
00:06:56,479 --> 00:07:02,160
an invalid entry in the gdt

202
00:07:00,160 --> 00:07:04,080
so it says right here that first

203
00:07:02,160 --> 00:07:05,680
descriptor and the gdt is not used and

204
00:07:04,080 --> 00:07:07,039
this is treated as invalid

205
00:07:05,680 --> 00:07:09,360
so that means for all intents and

206
00:07:07,039 --> 00:07:10,080
purposes right now on this Windows

207
00:07:09,360 --> 00:07:13,039
version

208
00:07:10,080 --> 00:07:14,720
the ldt is not being used at all and

209
00:07:13,039 --> 00:07:16,000
we'll talk a little bit later we'll put

210
00:07:14,720 --> 00:07:17,520
a reference to something where you can

211
00:07:16,000 --> 00:07:19,520
see a little bit more about how

212
00:07:17,520 --> 00:07:21,919
the ldt is or isn't used in different

213
00:07:19,520 --> 00:07:24,560
versions of Windows over time

214
00:07:21,919 --> 00:07:26,080
but now I just want to quick take issue

215
00:07:24,560 --> 00:07:28,240
with this picture from

216
00:07:26,080 --> 00:07:29,759
intel because to me it feels a little

217
00:07:28,240 --> 00:07:31,520
bit misleading because you've got this

218
00:07:29,759 --> 00:07:32,880
gdt you've got this ldt

219
00:07:31,520 --> 00:07:34,560
and there's this thing that looks like a

220
00:07:32,880 --> 00:07:36,800
pointer from the gdt

221
00:07:34,560 --> 00:07:38,080
into the ldt register as if you know

222
00:07:36,800 --> 00:07:40,960
this thing is

223
00:07:38,080 --> 00:07:41,520
you know pointing at the register itself

224
00:07:40,960 --> 00:07:45,120
so

225
00:07:41,520 --> 00:07:47,759
what's actually happening here

226
00:07:45,120 --> 00:07:49,280
is that if someone wrote to the ldt

227
00:07:47,759 --> 00:07:52,319
register specifically the segment

228
00:07:49,280 --> 00:07:54,720
selector the visible 16-bit portion

229
00:07:52,319 --> 00:07:56,720
what would happen is that that would

230
00:07:54,720 --> 00:07:59,039
cause the segment selector to be looking

231
00:07:56,720 --> 00:08:00,800
up some particular index in the gdt

232
00:07:59,039 --> 00:08:02,479
right now it's zero for us but if this

233
00:08:00,800 --> 00:08:04,080
was actually being used it would select

234
00:08:02,479 --> 00:08:05,280
some index from the gdt

235
00:08:04,080 --> 00:08:07,599
which would be pointing at some

236
00:08:05,280 --> 00:08:08,800
particular data structure in the ldt

237
00:08:07,599 --> 00:08:11,599
that talks about the

238
00:08:08,800 --> 00:08:12,000
size the base and the limit of the ldt

239
00:08:11,599 --> 00:08:14,160
so

240
00:08:12,000 --> 00:08:15,280
looking this up writing to the segment

241
00:08:14,160 --> 00:08:17,039
selector

242
00:08:15,280 --> 00:08:19,039
causes the information from this table

243
00:08:17,039 --> 00:08:20,720
to be cached into the hidden portion of

244
00:08:19,039 --> 00:08:23,599
the ldt register

245
00:08:20,720 --> 00:08:24,960
and now it will be pointing at the ldt

246
00:08:23,599 --> 00:08:26,639
itself

247
00:08:24,960 --> 00:08:27,919
so this is just kind of even though

248
00:08:26,639 --> 00:08:29,199
there's just this i know what they're

249
00:08:27,919 --> 00:08:30,080
trying to say here they're kind of

250
00:08:29,199 --> 00:08:31,680
trying to say well

251
00:08:30,080 --> 00:08:34,320
it's this hidden portion is filled in

252
00:08:31,680 --> 00:08:37,440
from the gdt but i want to be clear that

253
00:08:34,320 --> 00:08:38,240
gdtr is its own register ldtr is its own

254
00:08:37,440 --> 00:08:40,320
register

255
00:08:38,240 --> 00:08:41,839
and it is the act of writing information

256
00:08:40,320 --> 00:08:44,000
into here which causes

257
00:08:41,839 --> 00:08:46,080
caching of table information here but

258
00:08:44,000 --> 00:08:48,560
really it's you know this this register

259
00:08:46,080 --> 00:08:50,000
is what points at the ldt

260
00:08:48,560 --> 00:08:51,920
right now showing that slightly

261
00:08:50,000 --> 00:08:54,560
differently using the diagrams that i

262
00:08:51,920 --> 00:08:56,399
showed before if the ldt register the

263
00:08:54,560 --> 00:08:58,800
16-bit visible portion

264
00:08:56,399 --> 00:08:59,600
has a segment selector has some rpl i

265
00:08:58,800 --> 00:09:01,440
said that the

266
00:08:59,600 --> 00:09:03,200
table indicator must always point at the

267
00:09:01,440 --> 00:09:06,080
gdt because it can't be

268
00:09:03,200 --> 00:09:07,600
finding the ldt via itself and then

269
00:09:06,080 --> 00:09:09,120
let's just say the index you know

270
00:09:07,600 --> 00:09:10,800
pointed at three

271
00:09:09,120 --> 00:09:12,880
all right so index pointing at three

272
00:09:10,800 --> 00:09:13,920
would select some data structure from

273
00:09:12,880 --> 00:09:15,200
the gdt

274
00:09:13,920 --> 00:09:17,600
which we learned about in the next

275
00:09:15,200 --> 00:09:19,920
section and that would talk about you

276
00:09:17,600 --> 00:09:22,800
know here is the base here is the size

277
00:09:19,920 --> 00:09:23,680
of the ldt and then behind the scenes

278
00:09:22,800 --> 00:09:25,519
also

279
00:09:23,680 --> 00:09:27,600
this is filling in the hidden portion

280
00:09:25,519 --> 00:09:29,120
just caching the information about

281
00:09:27,600 --> 00:09:31,440
what is the base, what is the limit, what

282
00:09:29,120 --> 00:09:32,800
are the attributes of the ldt, so that

283
00:09:31,440 --> 00:09:34,959
the hidden portion

284
00:09:32,800 --> 00:09:36,000
when you look at the ldt register

285
00:09:34,959 --> 00:09:37,920
someone's accessing

286
00:09:36,000 --> 00:09:39,519
ldt information it's all just cached

287
00:09:37,920 --> 00:09:42,959
here in the register

288
00:09:39,519 --> 00:09:43,839
for looking up the ldt okay so one more

289
00:09:42,959 --> 00:09:46,399
time

290
00:09:43,839 --> 00:09:48,240
the table indicator the segment selector

291
00:09:46,399 --> 00:09:50,720
is going to be some sort of thing

292
00:09:48,240 --> 00:09:52,000
that is stored in a register like cs, ss

293
00:09:50,720 --> 00:09:52,560
and the segment registers we learned

294
00:09:52,000 --> 00:09:56,560
about

295
00:09:52,560 --> 00:09:58,080
or ldtr now we've learned about

296
00:09:56,560 --> 00:09:59,600
and we've got these special purpose

297
00:09:58,080 --> 00:10:00,959
registers that you know

298
00:09:59,600 --> 00:10:03,279
a table indicator might say it's

299
00:10:00,959 --> 00:10:05,120
pointing at some particular

300
00:10:03,279 --> 00:10:06,480
table but the special purpose registers

301
00:10:05,120 --> 00:10:08,800
are how the hardware actually finds

302
00:10:06,480 --> 00:10:11,519
those tables

303
00:10:08,800 --> 00:10:13,440
and then the tables themselves are a set

304
00:10:11,519 --> 00:10:15,920
of data structures that are stored

305
00:10:13,440 --> 00:10:15,920
in RAM

306
00:10:16,480 --> 00:10:20,959
so what ultimately is the point of the

307
00:10:18,480 --> 00:10:21,839
ldt well the original point was to

308
00:10:20,959 --> 00:10:23,920
basically give

309
00:10:21,839 --> 00:10:26,240
different processes their own different

310
00:10:23,920 --> 00:10:29,519
view of segmented memory

311
00:10:26,240 --> 00:10:32,640
because the segment selectors

312
00:10:29,519 --> 00:10:33,440
have a 13-bit index that means that you

313
00:10:32,640 --> 00:10:36,000
know you could have

314
00:10:33,440 --> 00:10:38,079
up to 8,000 different processes having

315
00:10:36,000 --> 00:10:39,839
their own different views of memory

316
00:10:38,079 --> 00:10:41,440
but practically speaking these days

317
00:10:39,839 --> 00:10:43,120
people don't use that for that

318
00:10:41,440 --> 00:10:44,800
they use paging which we'll learn about

319
00:10:43,120 --> 00:10:46,640
later in this class

320
00:10:44,800 --> 00:10:48,399
and so you know the only other comment

321
00:10:46,640 --> 00:10:50,800
about ldt's is that

322
00:10:48,399 --> 00:10:51,920
when amd made its 64-bit extensions the

323
00:10:50,800 --> 00:10:54,240
ldt was actually

324
00:10:51,920 --> 00:10:55,440
made a little bit more flexible because

325
00:10:54,240 --> 00:10:57,440
it could have

326
00:10:55,440 --> 00:10:58,880
these uh these data structures within it

327
00:10:57,440 --> 00:11:00,160
these segment descriptors which we

328
00:10:58,880 --> 00:11:03,120
learned about in the next section

329
00:11:00,160 --> 00:11:04,800
these data these data structures were

330
00:11:03,120 --> 00:11:07,440
extended to 64-bit

331
00:11:04,800 --> 00:11:08,800
uh base addresses of segments whereas

332
00:11:07,440 --> 00:11:11,360
the gdt was not

333
00:11:08,800 --> 00:11:13,200
and so essentially if you wanted to get

334
00:11:11,360 --> 00:11:15,279
code and data segments that you know

335
00:11:13,200 --> 00:11:17,680
pointed anywhere in memory

336
00:11:15,279 --> 00:11:19,519
or were more flexible in that way then

337
00:11:17,680 --> 00:11:21,440
you would have to use the ldt but

338
00:11:19,519 --> 00:11:23,760
like i said practically speaking no one

339
00:11:21,440 --> 00:11:25,920
actually uses it that way

340
00:11:23,760 --> 00:11:27,360
so there's a good blog post here which i

341
00:11:25,920 --> 00:11:28,079
recommend you check out which talks a

342
00:11:27,360 --> 00:11:31,040
little bit about

343
00:11:28,079 --> 00:11:32,000
how Windows used the ldt for user mode

344
00:11:31,040 --> 00:11:34,720
scheduling

345
00:11:32,000 --> 00:11:35,120
but ultimately it was removed in Windows

346
00:11:34,720 --> 00:11:37,600
10

347
00:11:35,120 --> 00:11:38,560
update at some point in favor of another

348
00:11:37,600 --> 00:11:40,720
mechanism

349
00:11:38,560 --> 00:11:42,720
which you know we'll actually see about

350
00:11:40,720 --> 00:11:44,399
later on in this class but this is good

351
00:11:42,720 --> 00:11:46,720
sort of historical perspective

352
00:11:44,399 --> 00:11:47,839
and it'll show just how you know you can

353
00:11:46,720 --> 00:11:50,240
only read certain

354
00:11:47,839 --> 00:11:51,680
you know os internals type blog posts if

355
00:11:50,240 --> 00:11:54,240
you have the kind of information that's

356
00:11:51,680 --> 00:11:56,720
talked about in this class.

357
00:11:54,240 --> 00:11:57,839
All right so here is this big picture of

358
00:11:56,720 --> 00:11:59,519
what we're going to learn in the class

359
00:11:57,839 --> 00:12:00,639
we saw our flags we saw segment

360
00:11:59,519 --> 00:12:03,200
selectors

361
00:12:00,639 --> 00:12:05,040
and now we just saw the notion of the

362
00:12:03,200 --> 00:12:06,880
global descriptor table we didn't cover

363
00:12:05,040 --> 00:12:09,519
what's inside of it yet

364
00:12:06,880 --> 00:12:10,079
and we also saw the notion of the gdt

365
00:12:09,519 --> 00:12:12,079
register

366
00:12:10,079 --> 00:12:13,600
points at the global descriptor table

367
00:12:12,079 --> 00:12:16,320
and so the next section will talk about

368
00:12:13,600 --> 00:12:18,160
the things that are inside of it.

369
00:12:16,320 --> 00:12:19,680
We also saw the notion of the local

370
00:12:18,160 --> 00:12:22,160
descriptor table

371
00:12:19,680 --> 00:12:23,600
and the local descriptor table register

372
00:12:22,160 --> 00:12:24,560
which points at the local descriptor

373
00:12:23,600 --> 00:12:27,040
table

374
00:12:24,560 --> 00:12:28,160
and we saw that there's notionally going

375
00:12:27,040 --> 00:12:31,440
to be some entry

376
00:12:28,160 --> 00:12:32,880
the ldtr has some index that it's

377
00:12:31,440 --> 00:12:36,000
selecting from the gdt

378
00:12:32,880 --> 00:12:36,320
which describes the ldt itself and sort

379
00:12:36,000 --> 00:12:39,519
of

380
00:12:36,320 --> 00:12:41,440
you know points back at to this ldt

381
00:12:39,519 --> 00:12:43,440
itself that probably should be pointing

382
00:12:41,440 --> 00:12:46,240
right here at the base of the lpt but

383
00:12:43,440 --> 00:12:46,240
whatever.


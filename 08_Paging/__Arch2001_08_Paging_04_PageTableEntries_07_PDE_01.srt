1
00:00:00,000 --> 00:00:03,280
all right we're over halfway there let's

2
00:00:01,439 --> 00:00:05,759
go let's keep going enhance

3
00:00:03,280 --> 00:00:07,040
pdes what are they what are they all

4
00:00:05,759 --> 00:00:10,080
about

5
00:00:07,040 --> 00:00:11,920
nobody knows all right pdes

6
00:00:10,080 --> 00:00:14,480
once again there's three but i leave off

7
00:00:11,920 --> 00:00:16,080
one present equals zero means it's going

8
00:00:14,480 --> 00:00:19,840
to cause errors when you try to access

9
00:00:16,080 --> 00:00:21,520
it ps equal one or zero once again gives

10
00:00:19,840 --> 00:00:24,320
us two different interpretations

11
00:00:21,520 --> 00:00:26,640
for ps page size equal one we've got

12
00:00:24,320 --> 00:00:28,400
something that has a two megabyte page

13
00:00:26,640 --> 00:00:30,240
frame so it's a large page again it's

14
00:00:28,400 --> 00:00:33,120
not a four kilobyte page

15
00:00:30,240 --> 00:00:33,920
so physical address of a large page ps

16
00:00:33,120 --> 00:00:36,559
equals zero

17
00:00:33,920 --> 00:00:38,320
it's going to point at another table i

18
00:00:36,559 --> 00:00:39,680
don't remember my animation so i'm just

19
00:00:38,320 --> 00:00:40,719
going to click through them and i might

20
00:00:39,680 --> 00:00:44,000
have to go backwards

21
00:00:40,719 --> 00:00:46,640
there we go all right page size and

22
00:00:44,000 --> 00:00:49,520
physical address of a two megabyte frame

23
00:00:46,640 --> 00:00:52,160
if page size is equal to one

24
00:00:49,520 --> 00:00:53,840
now just like before the if this

25
00:00:52,160 --> 00:00:55,039
particular thing is interpreted as a two

26
00:00:53,840 --> 00:00:58,160
megabyte page

27
00:00:55,039 --> 00:01:01,440
all of our friendly friends p read write

28
00:00:58,160 --> 00:01:02,960
us d g xd those are all interpreted the

29
00:01:01,440 --> 00:01:06,159
exact same as we've seen in

30
00:01:02,960 --> 00:01:09,360
other p star e's page

31
00:01:06,159 --> 00:01:10,640
something entries right and again intel

32
00:01:09,360 --> 00:01:13,600
was telling us this

33
00:01:10,640 --> 00:01:14,960
in this picture it was saying pde with

34
00:01:13,600 --> 00:01:17,200
ps equals one

35
00:01:14,960 --> 00:01:18,960
is going to point at a two megabyte page

36
00:01:17,200 --> 00:01:19,439
instead of a four kilobyte one instead

37
00:01:18,960 --> 00:01:21,360
of

38
00:01:19,439 --> 00:01:24,320
pointing at a sorry pointing at a page

39
00:01:21,360 --> 00:01:27,520
table that points at a four kilobyte one

40
00:01:24,320 --> 00:01:30,400
so if instead page size is equal to zero

41
00:01:27,520 --> 00:01:31,680
then this is going to be a page table

42
00:01:30,400 --> 00:01:34,240
physical address

43
00:01:31,680 --> 00:01:35,040
so the bottom 12 bits always treat it as

44
00:01:34,240 --> 00:01:37,920
zero

45
00:01:35,040 --> 00:01:39,680
and the upper end bits depending on how

46
00:01:37,920 --> 00:01:42,240
big your physical address spaces

47
00:01:39,680 --> 00:01:45,200
are treated as the physical address of

48
00:01:42,240 --> 00:01:45,200
the page table

